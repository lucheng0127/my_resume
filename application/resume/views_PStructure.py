# -*- coding: utf-8 -*-

from flask import render_template, redirect, url_for, flash, abort
from flask_login import current_user, login_required
from playhouse.flask_utils import get_object_or_404
from ..models import User, ProjectStructure
from .forms_PStructure import addProjectStructureForm, addProjectStructureDataForm
from . import bpResume

@bpResume.route('/addPStructure', methods=['GET', 'POST'])
def addPStructure():
    form = addProjectStructureForm()
    if form.validate_on_submit():
        query = (User.select(User.id, User.chinesename))
        users = [row for row in query]
        pExperienceForm = addProjectStructureDataForm()
        pExperienceDic = dict(
            name=form.name.data,
            pdate=form.pdate.data,
            user=current_user.id
        )
        pExperienceForm.user.choices = [(user.id, user.chinesename) for user in users]
        pExperienceForm.user.data = pExperienceDic['user']
        pExperienceForm.name.data = pExperienceDic['name']
        pExperienceForm.pdate.data = pExperienceDic['pdate']
        if not pExperienceForm.validate():
            return redirect(url_for('bpShow.resume'))
        ProjectStructure.create(**pExperienceDic)
        return redirect(url_for('bpShow.resume'))
    return render_template('show/resume/addProjectStructure.html', form=form)

@bpResume.route('/deletePStructure/<int:pStructureId>', methods=['GET', 'POST'])
@login_required
def deletePStructure(pStructureId):
    projectStructure = get_object_or_404(ProjectStructure, ProjectStructure.id == pStructureId)
    if not current_user.id == projectStructure.user.id:
        abort(403)
    projectStructure.delete_instance()
    return redirect(url_for('bpShow.resume'))