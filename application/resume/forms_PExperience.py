# -*- coding: utf-8 -*-

from flask_wtf import Form
from wtforms import TextAreaField, SelectField, StringField
from wtforms.validators import DataRequired

class addProjectExperienceForm(Form):
    name = StringField(
        u'项目名称',
        validators=[
            DataRequired(u'项目名称不能为空')
        ]
    )

    body = TextAreaField(
        u'描述',
        validators=[
            DataRequired(u'请描述您的经历')
        ]
    )

class addProjectExperienceDataForm(Form):
    name = StringField(
        u'项目名称',
        validators=[
            DataRequired(u'项目名称不能为空')
        ]
    )

    body = TextAreaField(
        u'描述',
        validators=[
            DataRequired(u'请描述您的经历')
        ]
    )

    user = SelectField(
        u'用户',
        validators=[
            DataRequired(u'用户不能为空')
        ]
    )