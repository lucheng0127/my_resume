# -*- coding: utf-8 -*-

from flask import render_template, redirect, url_for, flash, abort
from flask_login import current_user, login_required
from playhouse.flask_utils import get_object_or_404
from ..models import User, ProjectExperience
from .forms_PExperience import addProjectExperienceForm, addProjectExperienceDataForm
from . import bpResume

@bpResume.route('/addPExperience', methods=['GET', 'POST'])
def addPExperience():
    form = addProjectExperienceForm()
    if form.validate_on_submit():
        query = (User.select(User.id, User.chinesename))
        users = [row for row in query]
        pExperienceForm = addProjectExperienceDataForm()
        pExperienceDic = dict(
            name=form.name.data,
            body=form.body.data,
            user=current_user.id
        )
        pExperienceForm.user.choices = [(user.id, user.chinesename) for user in users]
        pExperienceForm.user.data = pExperienceDic['user']
        pExperienceForm.name.data = pExperienceDic['name']
        pExperienceForm.body.data = pExperienceDic['body']
        if not pExperienceForm.validate():
            return redirect(url_for('bpShow.resume'))
        ProjectExperience.create(**pExperienceDic)
        return redirect(url_for('bpShow.resume'))
    return render_template('show/resume/addProjectExperience.html', form=form)

@bpResume.route('/deletePExperience/<int:pExperienceId>', methods=['GET', 'POST'])
@login_required
def deletePExperience(pExperienceId):
    projectExperience = get_object_or_404(ProjectExperience, ProjectExperience.id == pExperienceId)
    if not current_user.id == projectExperience.user.id:
        abort(403)
    projectExperience.delete_instance()
    return redirect(url_for('bpShow.resume'))