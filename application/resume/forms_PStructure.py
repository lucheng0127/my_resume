# -*- coding: utf-8 -*-

from flask_wtf import Form
from wtforms import TextAreaField, SelectField, StringField
from wtforms.validators import DataRequired

class addProjectStructureForm(Form):
    name = StringField(
        u'技能',
        validators=[
            DataRequired(u'技能不能为空')
        ]
    )

    pdate = StringField(
        u'掌握度',
        validators=[
            DataRequired(u'掌握度不能为空')
        ]
    )

class addProjectStructureDataForm(Form):
    name = StringField(
        u'技能',
        validators=[
            DataRequired(u'技能不能为空')
        ]
    )

    pdate = StringField(
        u'掌握度',
        validators=[
            DataRequired(u'掌握度不能为空')
        ]
    )

    user = SelectField(
        u'用户',
        validators=[
            DataRequired(u'用户不能为空')
        ]
    )